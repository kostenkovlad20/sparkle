$(document).ready(function() {
    $('.owl-one').owlCarousel({
        loop:true,
        nav:true,
        pagination: true,
        dotsEach: true,
        margin:10,
        responsive:{
            0:{
                items:1,

            },
            600:{
                items:3,

            },
            1000:{
                items:5,

            }
        }
    });

    $('.owl-two').owlCarousel({
        loop:true,
        nav:true,
        pagination: true,
        dotsEach: true,
        margin:10,
        items:1
    })

    $('.owl-three').owlCarousel({
        loop:false,
        nav:false,
        dots: false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        items:1
    })

    $('.owl-four').owlCarousel({
        loop:true,
        nav:true,
        pagination: true,
        dotsEach: true,
        margin:10,
        responsive:{
            0:{
                items:1,

            },
            600:{
                items:3,

            },
            1000:{
                items:5,

            }
        }
    });

    $('.menu-icon').on('click', function() {
        $(this).toggleClass('menu-icon-active');
        $('.main-menu').toggleClass('main-menu-active');
    });

}); 